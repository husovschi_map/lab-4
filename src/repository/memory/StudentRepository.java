package repository.memory;

import domain.Student;

public class StudentRepository extends AbstractRepository<String, Student> {

    public StudentRepository() {
        super();
    }
}