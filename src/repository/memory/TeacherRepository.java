package repository.memory;

import domain.Teacher;

public class TeacherRepository extends AbstractRepository<String, Teacher> {

    public TeacherRepository() {
        super();
    }
}