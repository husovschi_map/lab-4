package repository.memory;

import domain.Grade;

public class GradeRepository extends AbstractRepository<String, Grade> {

    public GradeRepository() {
        super();
    }
}
