import domain.SchoolYear;
import interactive.UI;
import repository.memory.GradeRepository;
import repository.memory.HomeworkRepository;
import repository.memory.StudentRepository;
import repository.memory.TeacherRepository;
import service.GradeService;
import service.HomeworkService;
import service.StudentService;
import service.TeacherService;
import validator.GradeValidator;
import validator.HomeworkValidator;
import validator.StudentValidator;
import validator.TeacherValidator;

public class Main {

    public static void main(String[] args) {
        SchoolYear schoolYear = new SchoolYear();

        StudentValidator studentValidator = new StudentValidator();
        StudentRepository studentRepository = new StudentRepository();
        StudentService studentService = new StudentService(studentRepository, studentValidator);

        HomeworkValidator homeworkValidator = new HomeworkValidator();
        HomeworkRepository homeworkRepository = new HomeworkRepository();
        HomeworkService homeworkService = new HomeworkService(homeworkRepository, homeworkValidator, schoolYear);

        GradeValidator gradeValidator = new GradeValidator();
        GradeRepository gradeRepository = new GradeRepository();
        GradeService gradeService = new GradeService(gradeRepository, gradeValidator, schoolYear);

        TeacherValidator teacherValidator = new TeacherValidator();
        TeacherRepository teacherRepository = new TeacherRepository();
        TeacherService teacherService = new TeacherService(teacherRepository, teacherValidator);

        UI ui = new UI(gradeService, homeworkService, studentService, teacherService, schoolYear);
        ui.run();
    }
}
