package service;

import domain.Student;
import repository.Repository;
import validator.StudentValidator;
import validator.ValidationException;

public class StudentService {
    private Repository<String, Student> repository;
    private StudentValidator studentValidator;

    public StudentService(Repository<String, Student> repository, StudentValidator studentValidator) {
        this.repository = repository;
        this.studentValidator = studentValidator;
    }

    public void save(String id, String firstName, String lastName, String group, String email, String labTeacher) {
        Student student = new Student(id, firstName, lastName, group, email, labTeacher);
        try {
            this.studentValidator.validate(student);
            this.repository.save(student);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public Student findOne(String id) {
        return this.repository.findOne(id);
    }

    public Student delete(String id) {
        return this.repository.delete(id);
    }

    public Iterable<Student> findAll(String id) {
        return this.repository.findAll();
    }

    public void update(String id, String firstName, String lastName, String group, String email, String labTeacher) {
        Student student = this.repository.findOne(id);
        Student student1 = student;
        student1.setFirstName(firstName);
        student1.setLastName(lastName);
        student1.setGroup(group);
        student1.setEmail(email);
        student1.setLabTeacher(labTeacher);
        try {
            this.studentValidator.validate(student1);
            this.repository.update(student, student1);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public Iterable<Student> findAll() {
        return this.repository.findAll();
    }
}
