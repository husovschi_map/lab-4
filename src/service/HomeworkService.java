package service;

import domain.Homework;
import domain.SchoolYear;
import repository.Repository;
import validator.HomeworkValidator;
import validator.ValidationException;

public class HomeworkService {
    private Repository<String, Homework> repository;
    private HomeworkValidator validator;
    private SchoolYear schoolYear;

    public HomeworkService(Repository<String, Homework> repository, HomeworkValidator validator, SchoolYear schoolYear) {
        this.repository = repository;
        this.validator = validator;
        this.schoolYear = schoolYear;
    }

    public int maxGrade(String id, boolean lateTeacher, int assignmentPresentation) {
        int currentWeek = this.schoolYear.getCurrentWeek();

        if (lateTeacher)
            currentWeek = assignmentPresentation;
        // TODO may need to be fixed
        Homework homework = this.findOne(id);
        if (homework.getDeadLine() >= currentWeek) return 10;
        if (homework.getDeadLine() < currentWeek) return 10 - (currentWeek - homework.getDeadLine());
        return 4;
    }

    public Homework delete(String id) {
        return this.repository.delete(id);
    }

    public Homework findOne(String id) {
        return this.repository.findOne(id);
    }

    public Iterable<Homework> findAll() {
        return this.repository.findAll();
    }

    public void save(String id, String deadLine, String description) {
        Homework homework = new Homework(id, schoolYear.getCurrentWeek(), Integer.parseInt(deadLine), description);
        try {
            this.validator.validate(homework);
            this.repository.save(homework);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void update(String id, String deadLine, String description) {
        Homework homework = this.repository.findOne(id);
        Homework homework1 = homework;
        if (Integer.parseInt(deadLine) < this.schoolYear.getCurrentWeek())
            return;
        homework1.setDescription(description);
        homework1.setDeadLine(Integer.parseInt(deadLine));

        try {
            this.validator.validate(homework1);
            this.repository.update(homework, homework1);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}
