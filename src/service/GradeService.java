package service;

import domain.Grade;
import domain.SchoolYear;
import repository.Repository;
import validator.GradeValidator;
import validator.ValidationException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GradeService {
    private Repository<String, Grade> repository;
    private GradeValidator validator;
    private SchoolYear schoolYear;

    public GradeService(Repository<String, Grade> repository, GradeValidator validator, SchoolYear schoolYear) {
        this.repository = repository;
        this.validator = validator;
        this.schoolYear = schoolYear;
    }

    public List<Grade> gradeHomework(String idHomework) {
        List<Grade> gradeList = new ArrayList<Grade>();
        for (Grade grade : this.repository.findAll())
            gradeList.add(grade);
        return gradeList.stream()
                .filter(x -> x.getId().split(" ")[1].equals(idHomework))
                .collect(Collectors.toList());
    }

    public List<Grade> gradeAtHomeworkTeacher(String idHomework, String teacher) {
        List<Grade> gradeList = new ArrayList<Grade>();
        for (Grade grade : this.repository.findAll())
            gradeList.add(grade);
        return gradeList.stream()
                .filter(x -> x.getId().split(" ")[1].equals(idHomework) && x.getTeacher().equals(teacher))
                .collect(Collectors.toList());
    }

    public void save(String idStudent, String idHomework, String teacher, String value) {
        String id = idStudent + " " + idHomework;
        Grade grade = new Grade(id, LocalDateTime.now(), teacher, Integer.parseInt(value));
        try {
            this.validator.validate(grade);
            this.repository.save(grade);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void update(String idStudent, String idHomework, String value, String teacher) {
        String id = idStudent + " " + idHomework;
        Grade gradeToUpdate = this.repository.findOne(id);
        if (gradeToUpdate != null) {
            gradeToUpdate.setDate(LocalDateTime.now());
            gradeToUpdate.setTeacher(teacher);
            gradeToUpdate.setValue(Integer.parseInt(value));
            //TODO
        }
    }

    public Grade delete(String id) {
        return this.repository.delete(id);
    }

    public Grade findOne(String id) {
        return this.repository.findOne(id);
    }

    public Iterable<Grade> findAll() {
        return this.repository.findAll();
    }
}
