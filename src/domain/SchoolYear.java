package domain;

import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class SchoolYear {

    protected LocalDate startSemester;
    protected LocalDate startHoliday;
    protected LocalDate endHoliday;
    protected LocalDate endSemester;

    private static SchoolYear instance = null;

    public SchoolYear() {
    }

    public SchoolYear getInstance() {
        if (instance == null)
            instance = new SchoolYear();
        return instance;
    }

    // TODO Move validDate somewhere else
    public boolean validateDate(LocalDate localDate) {
        return localDate.isAfter(this.startHoliday) && localDate.isBefore(this.endSemester) &&
                (localDate.isBefore(this.startHoliday) || localDate.isAfter(this.endHoliday));
    }

    public int getCurrentWeek() {
        TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
        int currentWeek = LocalDate.now().get(woy);  // get current week in year
        int startHolidayWeek = startHoliday.get(woy);
        int startSemesterWeek = startSemester.get(woy);
        int endHolidayWeek = endHoliday.get(woy);

        if (currentWeek < startHolidayWeek)  // return current week in semester
            return currentWeek - startSemesterWeek + 1;
        else
            return currentWeek - startSemesterWeek + endHolidayWeek - startHolidayWeek;
    }

    public LocalDate getStartSemester() {
        return startSemester;
    }

    public void setStartSemester(LocalDate startSemester) {
        this.startSemester = startSemester;
    }

    public LocalDate getStartHoliday() {
        return startHoliday;
    }

    public void setStartHoliday(LocalDate startHoliday) {
        this.startHoliday = startHoliday;
    }

    public LocalDate getEndHoliday() {
        return endHoliday;
    }

    public void setEndHoliday(LocalDate endHoliday) {
        this.endHoliday = endHoliday;
    }

    public LocalDate getEndSemester() {
        return endSemester;
    }

    public void setEndSemester(LocalDate endSemester) {
        this.endSemester = endSemester;
    }
}
