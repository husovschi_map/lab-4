package service;

import domain.Teacher;
import org.junit.Test;
import repository.Repository;
import repository.memory.TeacherRepository;

import static org.junit.Assert.*;

public class TeacherServiceTest {
    private Repository<String, Teacher> repository = new TeacherRepository();
    private Teacher teacher1 = new Teacher("1", "a", "a", "a");
    private Teacher teacher2 = new Teacher("2", "a", "a", "a");
    private Teacher teacher3 = new Teacher("3", "a", "a", "a");
    private Teacher teacher4 = new Teacher("4", "a", "a", "a");

    @Test
    public void save() {
        this.repository.save(teacher1);
        assert (this.repository.findAll() != null);
    }

    @Test
    public void update() {
        this.repository.save(teacher2);
        Teacher teacher = this.teacher2;
        teacher.setLastName("b");
        this.repository.update(teacher2, teacher);
        assert (this.repository.findOne(teacher2.getId()).getLastName().equals("b"));
    }

    @Test
    public void findOne() {
        this.repository.save(teacher3);
        assert(this.repository.findOne(teacher3.getId()) != null);
    }

    @Test
    public void findAll() {
        this.repository.save(teacher1);
        this.repository.save(teacher2);
        this.repository.save(teacher3);
        this.repository.save(teacher4);
        assert (this.repository.findAll() != null);
    }
}