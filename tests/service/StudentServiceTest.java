package service;

import domain.Student;
import org.junit.Test;
import repository.Repository;
import repository.memory.StudentRepository;
import validator.StudentValidator;
import validator.ValidationException;

import static org.junit.Assert.*;

public class StudentServiceTest {
    private Repository<String, Student> repository = new StudentRepository();
    private StudentValidator validator = new StudentValidator();

    @Test
    public void save() {
        Student student = new Student("1", "firstname", "lastname", "group", "email", "labteacher");
        try {
            this.validator.validate(student);
            this.repository.save(student);
        } catch (ValidationException e) {
            assert(false);
        }
    }

    @Test
    public void findOne() {
        Student student = new Student("2", "firstname", "lastname", "group", "email", "labteacher");
        try {
            this.validator.validate(student);
            this.repository.save(student);
        } catch (ValidationException e) {
            assert (false);
        }
        assert (this.repository.findOne("2") != null);
    }

    @Test
    public void delete() {
        Student student = new Student("3", "firstname", "lastname", "group", "email", "labteacher");
        try {
            this.validator.validate(student);
            this.repository.save(student);
        } catch (ValidationException e) {
            assert (false);
        }
        assert(this.repository.delete("3") != null);
    }

    @Test
    public void findAll() {
        Student student1 = new Student("3", "firstname", "lastname", "group", "email", "labteacher");
        Student student2 = new Student("3", "firstname", "lastname", "group", "email", "labteacher");
        Student student3 = new Student("3", "firstname", "lastname", "group", "email", "labteacher");
        try {
            this.validator.validate(student1);
            this.validator.validate(student2);
            this.validator.validate(student3);
            this.repository.save(student1);
            this.repository.save(student3);
            this.repository.save(student3);
        } catch (ValidationException e) {
            assert (false);
        }
        assert (this.repository.findAll() != null);
    }

    @Test
    public void update() {
    }

}