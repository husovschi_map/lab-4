package service;

import domain.Grade;
import domain.SchoolYear;
import org.junit.Test;
import repository.Repository;
import repository.memory.GradeRepository;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class GradeServiceTest {
    private Repository<String, Grade> repository = new GradeRepository();
    private SchoolYear univYearStructure = new SchoolYear();
    private LocalDateTime localDateTime = LocalDateTime.of(2019, 10, 10, 10, 10);

    @Test
    public void save() {
        Grade grade = new Grade("1 1", localDateTime, "a", 10);
        this.repository.save(grade);
        assert (this.repository.findAll() != null);
    }

    @Test
    public void update() {
        Grade grade = new Grade ("1 1",  localDateTime, "a", 10);
        this.repository.save(grade);
        Grade grade2 = grade;
        grade2.setTeacher("b");
        assert (this.repository.findOne(grade.getId()).getId().equals(grade.getId()));
        this.repository.update(grade, grade2);
        assert (this.repository.findOne(grade2.getId()).getTeacher().equals("b"));
    }

    @Test
    public void delete() {
        Grade grade = new Grade ("1 1", localDateTime, "a", 10);
        this.repository.save(grade);
        assert (this.repository.findAll() != null);
    }

    @Test
    public void findOne() {
        Grade grade = new Grade ("1 1", localDateTime, "a", 10);
        this.repository.save(grade);
        assert (this.repository.findOne(grade.getId()) != null );
    }

    @Test
    public void findAll() {
        Grade grade = new Grade ("1 1", localDateTime, "a", 10);
        this.repository.save(grade);
        Grade grade2 = new Grade ("1 1", localDateTime, "a", 10);
        this.repository.save(grade2);
        assert(this.repository.findAll() != null);
    }
}