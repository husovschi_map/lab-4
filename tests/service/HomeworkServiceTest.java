package service;

import domain.Homework;
import domain.SchoolYear;
import org.junit.Test;
import repository.Repository;
import repository.memory.HomeworkRepository;
import validator.HomeworkValidator;

import java.util.Scanner;

import static org.junit.Assert.*;

public class HomeworkServiceTest {
    private Repository<String, Homework> repository = new HomeworkRepository();
    private HomeworkValidator validator = new HomeworkValidator();
    private SchoolYear schoolYear = new SchoolYear();

    @Test
    public void delete() {
        int deadlineWeek = this.schoolYear.getCurrentWeek();
        if (deadlineWeek != 14)
            deadlineWeek++;
        Homework homework = new Homework("1", this.schoolYear.getCurrentWeek(), deadlineWeek,"a");
        this.repository.save(homework);
        assert (this.repository.delete(homework.getId()) != null);
    }

    @Test
    public void findOne() {
        int deadlineWeek = this.schoolYear.getCurrentWeek();
        if (deadlineWeek != 14)
            deadlineWeek++;
        Homework homework = new Homework("1", this.schoolYear.getCurrentWeek(), deadlineWeek,"a");
        this.repository.save(homework);
        assert (this.repository.findOne(homework.getId()) != null);
    }

    @Test
    public void findAll() {
        int deadlineWeek = this.schoolYear.getCurrentWeek();
        if (deadlineWeek != 14)
            deadlineWeek++;
        Homework homework = new Homework("1", this.schoolYear.getCurrentWeek(), deadlineWeek, "a");
        Homework homework1 = new Homework(
                "2", this.schoolYear.getCurrentWeek(), deadlineWeek,"a");
        this.repository.save(homework);
        this.repository.save(homework1);
        assert (this.repository.findAll()  != null);
    }

    @Test
    public void save() {
        int deadlineWeek = this.schoolYear.getCurrentWeek();
        if (deadlineWeek != 14)
            deadlineWeek++;
        Homework homework = new Homework("1", this.schoolYear.getCurrentWeek(), deadlineWeek,"a");
        this.repository.save(homework);
        assert (this.repository.findAll() != null);
    }

    @Test
    public void update() {
        int deadlineWeek = this.schoolYear.getCurrentWeek();
        if (deadlineWeek != 14)
            deadlineWeek++;
        Homework homework = new Homework("1", this.schoolYear.getCurrentWeek(), deadlineWeek, "a");
        this.repository.save(homework);
        assert (this.repository.findOne(homework.getId()).getId().equals(homework.getId()));
        Homework homework2 = homework;
        homework2.setDescription("b");
        this.repository.update(homework, homework2);
        assert (this.repository.findOne(homework.getId()).getDescription().equals(homework2.getDescription()));
    }
}